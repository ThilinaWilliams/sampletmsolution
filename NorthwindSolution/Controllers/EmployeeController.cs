﻿using NorthwindSolution.IServices;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NorthwindSolution.Controllers
{
    /// <summary>
    /// Implements Employee Controler
    /// </summary>
    [RoutePrefix("api/employee")]
    public class EmployeeController : ApiController
    {
        #region Private Variables
        private readonly IEmployeeService empService;
        #endregion

        #region Public Constructor
        public EmployeeController(IEmployeeService empService)
        {
            this.empService = empService;
        }
        #endregion

        #region Public Methods
        
        /// <summary>
        /// Get all employee details
        /// </summary>
        /// <returns>returns a list of employees</returns>
        [HttpGet]
        [Route("GetEmployees")]
        public HttpResponseMessage GetEmployees()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, empService.GetEmployees());
            }
            catch (Exception ex)
            {

                //use nLog to log error here
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }
        } 
        #endregion
    }
}
